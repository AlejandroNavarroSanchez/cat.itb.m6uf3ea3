package models;

import java.util.List;
import java.io.Serializable;

public class BookDTO implements Serializable {
	private int _id;
	private String title;
	private String isbn;
	private int pageCount;
	private PublishedDateDTO publishedDate;
	private String thumbnailUrl;
	private String shortDescription;
	private String longDescription;
	private String status;
	private List<String> authors;
	private List<String> categories;

	// Getters
	public int get_id(){
		return _id;
	}
	public String getTitle(){
		return title;
	}
	public String getIsbn(){
		return isbn;
	}
	public int getPageCount(){
		return pageCount;
	}
	public PublishedDateDTO getPublishedDate(){
		return publishedDate;
	}
	public String getThumbnailUrl(){
		return thumbnailUrl;
	}
	public String getShortDescription(){
		return shortDescription;
	}
	public String getLongDescription(){
		return longDescription;
	}
	public String getStatus(){
		return status;
	}
	public List<String> getAuthors(){
		return authors;
	}
	public List<String> getCategories(){
		return categories;
	}

	// Setters
	public void set_id(int _id){
		this._id = _id;
	}
	public void setTitle(String title){
		this.title = title;
	}
	public void setIsbn(String isbn){
		this.isbn = isbn;
	}
	public void setPageCount(int pageCount){
		this.pageCount = pageCount;
	}
	public void setPublishedDate(PublishedDateDTO publishedDate){
		this.publishedDate = publishedDate;
	}
	public void setThumbnailUrl(String thumbnailUrl){
		this.thumbnailUrl = thumbnailUrl;
	}
	public void setShortDescription(String shortDescription){
		this.shortDescription = shortDescription;
	}
	public void setLongDescription(String longDescription){
		this.longDescription = longDescription;
	}
	public void setStatus(String status){
		this.status = status;
	}
	public void setAuthors(List<String> authors){
		this.authors = authors;
	}
	public void setCategories(List<String> categories){
		this.categories = categories;
	}

	@Override
 	public String toString(){
		return 
			"BookDTO{" + 
			"_id = '" + _id + '\'' +
			",title = '" + title + '\'' + 
			",isbn = '" + isbn + '\'' + 
			",pageCount = '" + pageCount + '\'' + 
			",publishedDate = '" + publishedDate + '\'' + 
			",thumbnailUrl = '" + thumbnailUrl + '\'' + 
			",shortDescription = '" + shortDescription + '\'' + 
			",longDescription = '" + longDescription + '\'' + 
			",status = '" + status + '\'' + 
			",authors = '" + authors + '\'' + 
			",categories = '" + categories + '\'' + 
			"}";
		}
}