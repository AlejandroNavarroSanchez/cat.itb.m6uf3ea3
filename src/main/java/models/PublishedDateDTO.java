package models;

import java.io.Serializable;

public class PublishedDateDTO implements Serializable {
	private String date;

	// Getters
	public String getDate(){
		return date;
	}

	// Setters
	public void setDate(String date){
		this.date = date;
	}

	@Override
 	public String toString(){
		return 
			"PublishedDateDTO{" + 
			"$date = '" + date + '\'' + 
			"}";
		}
}