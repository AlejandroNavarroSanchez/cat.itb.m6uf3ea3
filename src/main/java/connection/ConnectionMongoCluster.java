package connection;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;

public class ConnectionMongoCluster {
    static String user = "kappacat";
    static String pass = "kappacat";
    static String host = "clusterea1.zphho.mongodb.net";

    private static MongoClient mongoClient;

    public static MongoDatabase getDatabase(String database){
        mongoClient = MongoClients.create("mongodb+srv://"+user+":"+pass+"@"+host+"/retryWrites=true&w=majority");
        return mongoClient.getDatabase(database);
    }
    public static void closeConnection(){
        mongoClient.close();
    }

}
