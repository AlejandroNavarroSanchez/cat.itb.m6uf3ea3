package CRUD;

import com.google.gson.Gson;
import com.mongodb.MongoClient;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.Indexes;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import models.BookDTO;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import static com.mongodb.client.model.Accumulators.sum;
import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Sorts.ascending;
import static com.mongodb.client.model.Sorts.descending;
import static com.mongodb.client.model.Updates.*;
import static java.util.Arrays.asList;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class BookCRUD {
    public void dropBooks(MongoDatabase db) {
        db.getCollection("books").drop();
    }

    public BookDTO[] getBooksFromFile(String file) {
        StringBuilder stringFile = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                stringFile.append(linea);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        Gson gson = new Gson();
        return gson.fromJson(stringFile.toString(), BookDTO[].class);
    }

    public void insertObjects(BookDTO[] books, MongoDatabase db) {
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClient.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        db = db.withCodecRegistry(pojoCodecRegistry);

        // Collection
        MongoCollection<BookDTO> col = db.getCollection("books", BookDTO.class);

        for (BookDTO book : books) {
            System.out.println("Adding... " + book);
            col.insertOne(book);
        }
    }

    public void getLongDescriptions(MongoDatabase db) {
        db.getCollection("books").find()
                .projection(new Document("_id", 0)
                        .append("longDescription", 1))
                .forEach( d -> System.out.println(d.toJson()));
    }

    public void getTitleAndAuthor(MongoDatabase db, String author) {
        db.getCollection("books").find(in("authors", author))
                .projection(new Document("_id", 0)
                        .append("title", 1)
                        .append("authors", 1))
                .forEach( d -> System.out.println(d.toJson()));
    }

    public void getTitleAuthorsAndPageCount(MongoDatabase db, int min, int max, String cat) {
        db.getCollection("books").find(
                and(
                        gte("pageCount", min),
                        lte("pageCount", max),
                        in("categories", cat))
                )
                .projection(new Document("_id", 0)
                        .append("title", 1)
                        .append("authors", 1)
                        .append("pageCount", 1)
                )
                .forEach(d -> System.out.println(d.toJson()));
    }

    public void getTitleAndAuthorByArray(MongoDatabase db, String[] authors) {
        db.getCollection("books").find(
                and(
                        in("authors", authors[0]),
                        in("authors", authors[1]))
                )
                .projection(new Document("_id", 0)
                        .append("title", 1)
                        .append("authors", 1)
                )
                .forEach( d -> System.out.println(d.toJson()));
    }

    public void getCategoriesAndDiscardAuthor(MongoDatabase db, String[] categories, String author) {
        db.getCollection("books").find(
                and(
                        in("categories", categories[0]),
                        nin("authors", author))
                )
                .sort(ascending("title"))
                .forEach( d -> System.out.println(d.toJson()));
    }

    public void getByCategoriesAndLimit(MongoDatabase db, String[] categories, int limit) {
        db.getCollection("books").find(
                or(
                        in("categories", categories[0]),
                        in("categories", categories[1])
                ))
                .sort(ascending("title"))
                .limit(limit)
                .forEach(d -> System.out.println(d.toJson())
                );
    }

    public void getAllCategories(MongoDatabase db) {
        db.getCollection("books").distinct("categories", String.class).forEach(System.out::println);
    }

    public void getAllAuthors(MongoDatabase db) {
        db.getCollection("books").distinct("authors", String.class).forEach(System.out::println);
    }

    public long setAssessment(MongoDatabase db, int value) {
        MongoCollection<Document> col = db.getCollection("books");

        UpdateResult updateResult = col.updateMany(
                and(
                        in("categories", "Internet"),
                        in("status", "PUBLISH")
                ), set("Assessment", value)
        );

        return updateResult.getModifiedCount();
    }

    public long addAuthor(MongoDatabase db, String author, String title) {
        MongoCollection<Document> col = db.getCollection("books");

        UpdateResult updateResult = col.updateMany(
                eq("title", title), addToSet("authors", author)
        );

        return updateResult.getModifiedCount();
    }

    public void createUniqueKeyIndex(MongoDatabase db, String key) {
        db.getCollection("books").createIndex(Indexes.ascending(key), new IndexOptions().unique(true));
    }

    public void showAllIndexes(MongoDatabase db) {
        MongoCollection<Document> col = db.getCollection("books");
        for ( Document index : col.listIndexes() ) {
            System.out.println(index.toJson());
        }
    }

    public void removeBooksByAuthor(MongoDatabase db, String author) {
        DeleteResult deleteResult = db.getCollection("books").deleteMany(in("authors", author));
        System.out.println("Deleted books from collection: " + deleteResult.getDeletedCount());
    }

    public void removeBooksByTitle(MongoDatabase db, String title) {
        DeleteResult deleteResult = db.getCollection("books").deleteMany(eq("title", title));
        System.out.println("Deleted books from collection: " + deleteResult.getDeletedCount());
    }

    public void removeField(MongoDatabase db, String field) {
        UpdateResult updateResult = db.getCollection("books").updateMany(new Document(), unset(field));
        System.out.println("Deleted field \"" + field + "\" from collection.");
        System.out.println("Deleted fields: " + updateResult.getModifiedCount());
    }

    public void booksCountByAuthor(MongoDatabase db) {
        MongoCollection<Document> collection = db.getCollection("books");

        AggregateIterable<Document> resum = collection.aggregate(
                asList(unwind("$authors"),
                        group("$authors", sum("totalBooks", 1)),
                        sort(ascending("_id"))));

        resum.forEach(nom -> System.out.println(nom.toJson()));
    }
}
