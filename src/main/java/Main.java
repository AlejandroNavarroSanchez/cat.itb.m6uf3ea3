import CRUD.BookCRUD;
import com.mongodb.client.MongoDatabase;
import connection.ConnectionMongoCluster;
import models.BookDTO;

public class Main {
    private static final BookCRUD bookCRUD = new BookCRUD();

    public static void main(String[] args) {
        // Database
        MongoDatabase db = ConnectionMongoCluster.getDatabase("ITB");

        // EX 1
//        ex1(db, "src/main/resources/books.json");

        // EX 2
//        ex2_1(db);
//        ex2_2(db);
//        ex2_3(db, 300, 350, "Java");
//        ex2_4(db, new String[] {"Charlie Collins", "Robi Sen"});
//        ex2_5(db, new String[] {"Java"}, "Vikram Goyal");
//        ex2_6(db, new String[] {"Mobile", "Java"}, 15);
//        ex2_7(db);
//        ex2_8(db);

        // EX 3
//         ex3_1(db, 5000);
//         ex3_2(db, "Roger Whatch", "Unlocking Android");

        // EX 4
//        ex4(db, "isbn");

        // EX 5
//        ex5_1(db, "Kyle Baley");
//        ex5_2(db, "Coffeehouse");
//        ex5_3(db, "status");

        // EX 6
        ex6(db);

        ConnectionMongoCluster.closeConnection();
    }

    private static void ex6(MongoDatabase db) {
        bookCRUD.booksCountByAuthor(db);
    }

    private static void ex5_3(MongoDatabase db, String field) {
        bookCRUD.removeField(db, field);
    }

    private static void ex5_2(MongoDatabase db, String title) {
        bookCRUD.removeBooksByTitle(db, title);
    }

    private static void ex5_1(MongoDatabase db, String author) {
        bookCRUD.removeBooksByAuthor(db, author);
    }

    private static void ex4(MongoDatabase db, String key) {
        bookCRUD.createUniqueKeyIndex(db, key);
        bookCRUD.showAllIndexes(db);
    }

    private static void ex3_2(MongoDatabase db, String author, String title) {
        long l = bookCRUD.addAuthor(db, author, title);
        System.out.println("Registres actualitzats: " + l);
    }

    private static void ex3_1(MongoDatabase db, int value) {
        long l = bookCRUD.setAssessment(db, value);
        System.out.println("Registres actualitzats: " + l);
    }

    private static void ex2_8(MongoDatabase db) {
        bookCRUD.getAllAuthors(db);
    }

    private static void ex2_7(MongoDatabase db) {
        bookCRUD.getAllCategories(db);
    }

    private static void ex2_6(MongoDatabase db, String[] categories, int limit) {
        bookCRUD.getByCategoriesAndLimit(db, categories, limit);
    }

    private static void ex2_5(MongoDatabase db, String[] categories, String author) {
        bookCRUD.getCategoriesAndDiscardAuthor(db, categories, author);
    }

    private static void ex2_4(MongoDatabase db, String[] authors) {
        bookCRUD.getTitleAndAuthorByArray(db, authors);
    }

    private static void ex2_3(MongoDatabase db, int min, int max, String cat) {
        bookCRUD.getTitleAuthorsAndPageCount(db, min, max, cat);
    }

    public static void ex2_2(MongoDatabase db) {
        bookCRUD.getTitleAndAuthor(db, "Danno Ferrin");
    }

    public static void ex2_1(MongoDatabase db) {
        bookCRUD.getLongDescriptions(db);
    }

    public static void ex1(MongoDatabase db, String file) {
        // Drop collection
        bookCRUD.dropBooks(db);
        // Get file array
        BookDTO[] books = bookCRUD.getBooksFromFile(file);
        // Insert pojo array into collection
        bookCRUD.insertObjects(books, db);
    }
}
